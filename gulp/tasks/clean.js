var gulp = require('gulp'),
rimraf = require('gulp-rimraf'),
settings = require('../settings');
gulp.task('clean', function() {
  return gulp.src(settings.CLEAN.SRC, { read: false })
    .pipe(rimraf());
});