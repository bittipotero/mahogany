var gulp = require('gulp'),
 
// This will keeps pipes working after error event
  plumber = require('gulp-plumber'),
 
// linting
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
 
// Used in linting custom reporter
  map = require('map-stream'),
  events = require('events'),
  notify = require('gulp-notify'),
  emmitter = new events.EventEmitter(),
  path = require('path'),
  errorMessage = "",
  settings = require('../settings');
 
// Custom linting reporter used for error notify
var jsHintErrorReporter = map(function (file, cb) {
  if (!file.jshint.success) {
	  var res = file.jshint.results;
	
	for(var i = 0; i < res.length; i++){
		
			
			var x = [
			path.basename(file.path),
			res[i].error.line,
			res[i].error.reason
			];
			
			errorMessage = x;
			emmitter.emit('error', new Error());

	}
  }
	cb(null, file);
});
	

gulp.task('jshint', function () {
  gulp.src(settings.JSHINT.SRC)
    //.pipe(watch('client/js/**/*.js'))
    .pipe(plumber())
    .pipe(jshint('.jshintrc', {fail: true}))
    .pipe(jshint.reporter(stylish)) // Console output
    .pipe(jsHintErrorReporter) // If error pop up a notify alert
    .on('error', notify.onError(function () {
      var msgs = {message:errorMessage[2], title:errorMessage[0] + " line " + errorMessage[1]};
	  return msgs;
    }));

});







