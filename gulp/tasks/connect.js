var gulp = require('gulp'),
settings = require('../settings'),
connect = require('gulp-connect'),
os = require('os'),
open = require('open');

gulp.task('connect', function () {
  connect.server({
    root: settings.SERVER.ROOT,
    port: settings.SERVER.PORT,
    livereload: settings.SERVER.LIVERELOAD
  });

  if(settings.SERVER.OPEN){
  var browser = os.platform() === 'linux' ? 'google-chrome' : (
  os.platform() === 'darwin' ? 'google chrome' : (
  os.platform() === 'win32' ? 'chrome' : 'firefox'));
  open('http://localhost:3000', browser);
  }

});
