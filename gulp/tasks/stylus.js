var gulp = require('gulp'),
stylus = require('gulp-stylus'),
notify = require('gulp-notify'),
path = require('path'),
settings = require('../settings'),
connect = require('gulp-connect');


gulp.task('stylus', function () {
    gulp.src(settings.STYLES.SRC)
      .pipe(stylus())
      .on('error', notify.onError(function (error) {
            var msg = error.message.split('\n');
            console.log(path.basename(msg[0]));
            return { title: 'Error in ' + path.basename(msg[0]), message: error.name };
      }))
      .pipe(gulp.dest(settings.STYLES.DEST))
      .pipe(connect.reload());
});