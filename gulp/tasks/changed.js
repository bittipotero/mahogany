var gulp = require('gulp'),
settings = require('../settings'),
changed = require('gulp-changed'),
connect = require('gulp-connect');

gulp.task('changed', function () {
    return gulp.src(settings.RESOURCES.SRC)
        .pipe(changed(settings.RESOURCES.DEST))
        .pipe(gulp.dest(settings.RESOURCES.DEST))
        .pipe(connect.reload());
});