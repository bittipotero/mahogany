var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var debowerify = require('debowerify');
var path = require('path');
var notify = require('gulp-notify');
var settings = require('../settings');
var connect = require('gulp-connect');



    gulp.task('browserify', function() {

    var bundleStream = browserify(settings.BROWSERIFY.SRC).

        transform(debowerify).
        bundle()
        .on('error', notify.onError(function(err){
            console.log(err.message);
            var msg = err.message;
            var line = msg.split(' ')[1].slice(0, - 1);
            return {
                title: path.basename(err.filename) + " line " + line,
                message: msg
            };
        }));

    bundleStream.
        pipe(source('bundle.js')).
        pipe(gulp.dest(settings.BROWSERIFY.DEST)).
        pipe(connect.reload());

    });








