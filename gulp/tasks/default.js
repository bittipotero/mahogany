var gulp = require("gulp"),
settings = require('../settings'),
watch = require('gulp-watch'),
batch = require('gulp-batch');

gulp.task('default',['browserify','stylus','changed','copy','connect'], function(){
	
    watch(settings.BROWSERIFY.WATCH.SRC, batch(function (events, done) {
        gulp.start(settings.BROWSERIFY.WATCH.TASKS, done);
    }));
	 watch(settings.STYLES.WATCH.SRC, batch(function (events, done) {
	        gulp.start(settings.STYLES.WATCH.TASKS, done);
	}));
 	watch(settings.RESOURCES.WATCH.SRC, batch(function (events, done) {
	        gulp.start(settings.RESOURCES.WATCH.TASKS, done);
	}));


});