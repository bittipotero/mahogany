//tähän voisi kokeilla module.exports.settings (joku nimi) = { muut koodi}

var 

BUILD = {
	DEST: './dist'
},

SERVER = {
	PORT: '3000', 
	LIVERELOAD: true,
	ROOT: BUILD.DEST, 
	OPEN: true 
},

BROWSERIFY = {
	SRC: 'client/js/app.js',
	DEST: BUILD.DEST,
	WATCH: {
		SRC: ['client/js/**/*.js'],
		TASKS: ['browserify']
	}
},

STYLES = {
	SRC: ['client/styles/main.styl'],
	DEST: BUILD.DEST,
	WATCH: {
		SRC: ['client/styles/**/*.styl'],
		TASKS: ['stylus']
	}
},

RESOURCES = {
	SRC: ['index.html','client/**/*', '!client/js/**/*', '!client/styles/**/*', '!client/js','!client/styles'],
	DEST: BUILD.DEST,
	WATCH: {
		SRC: ['index.html','client/**/*', '!client/js/**/*', '!client/styles/**/*', 'client/js','client/styles'],
		TASKS: ['copy']
	}
},

JSHINT = {
	SRC: ['client/js/**/*.js']
},

CLEAN = {
	SRC: BUILD.DEST
};


module.exports = {
	BUILD:BUILD,
	SERVER: SERVER,
	BROWSERIFY: BROWSERIFY,
	STYLES:STYLES,
	RESOURCES:RESOURCES,
	CLEAN:CLEAN,
	JSHINT:JSHINT
};

